const { ethers } = require('hardhat')
const { assert } = require('chai')

describe('SimpleStorage', () => {
  let SimpleStorageFactory = null,
    simpleStorage = null

  beforeEach(async () => {
    SimpleStorageFactory = await ethers.getContractFactory('SimpleStorage')
    simpleStorage = await SimpleStorageFactory.deploy()
  })

  it('Should start with favorite number of 0', async () => {
    const expectedValue = '0',
      currentValue = await simpleStorage.retrieve()

    assert.equal(currentValue.toString(), expectedValue)
  })

  it('Should update when we call store', async () => {
    const expectedValue = '42',
      transactionResponse = await simpleStorage.store(expectedValue)

    await transactionResponse.wait(1)

    const updatedValue = await simpleStorage.retrieve()

    assert.equal(updatedValue.toString(), expectedValue)
  })
})
