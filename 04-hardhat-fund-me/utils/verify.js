// imports
const { run } = require('hardhat')

const verify = async (address, constructorArguments) => {
  console.log('Verifying contract...')

  try {
    await run('verify:verify', {
      address,
      constructorArguments,
    })
  } catch (ex) {
    if (ex.message.toLowerCase().includes('already verified')) {
      console.log('Already Verified!')
    } else {
      console.log(ex)
    }
  }
}

module.exports = { verify }
