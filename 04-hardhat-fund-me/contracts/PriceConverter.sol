// SPDX-License-Identifier: BSD2
pragma solidity ^0.8.0;

import "@chainlink/contracts/src/v0.8/interfaces/AggregatorV3Interface.sol";

library PriceConverter {
    function getPrice(AggregatorV3Interface priceFeed) internal view returns(uint256) {
        ( //uint80 roundId
        , int256 price
        , //uint startedAt
        , //uint timeStamp
        , //uint80 answeredInRound
        ) = priceFeed.latestRoundData();

        return uint256(price * 1e10); // 10^10
   }

    function getConversionRate(uint256 _ethAmount, AggregatorV3Interface _priceFeed) internal view returns(uint256) {
        uint256 ethPrice = getPrice(_priceFeed);
        uint256 ethAmountInUsd = (ethPrice * _ethAmount) / 1e18;

        return ethAmountInUsd;
    }
}