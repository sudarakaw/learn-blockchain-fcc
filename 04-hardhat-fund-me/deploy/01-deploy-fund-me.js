// imports
const {
    networkConfig,
    developmentChains,
  } = require('../hardhat.config.helper'),
  { network } = require('hardhat'),
  { verify } = require('../utils/verify')

module.exports = async (/* hre */ { getNamedAccounts, deployments }) => {
  const { deploy, log } = deployments,
    { deployer } = await getNamedAccounts(),
    chainId = network.config.chainId

  let ethUsdPriceFeedAddress

  if (developmentChains.includes(network.name)) {
    const ethUsdAggregator = await deployments.get('MockV3Aggregator')

    ethUsdPriceFeedAddress = ethUsdAggregator.address
  } else {
    ethUsdPriceFeedAddress = networkConfig[chainId]['ethUsdPriceFeedAddress']
  }

  const args = [ethUsdPriceFeedAddress],
    fundMe = await deploy('FundMe', {
      contract: 'FundMe',
      from: deployer,
      args,
      log: true,
      waitConfirmations: network.config.blockConfirmations || 1,
    })

  if (
    !developmentChains.includes(network.name) &&
    process.env.ETHERSCAN_API_KEY
  ) {
    // console.log('Waiting for block transactions...')
    // await fundMe.deployTransaction.wait(6)

    await verify(fundMe.address, args)
  }

  log('----------------------------------------')
}

module.exports.tags = ['all', 'fundme']
