const { deployments, ethers, getNamedAccounts } = require('hardhat'),
  { developmentChains } = require('../../hardhat.config.helper'),
  { assert, expect } = require('chai')

!developmentChains.includes(network.name)
  ? describe.skip
  : describe('FundMe', () => {
      let fundMe, deployer, mockV3Aggregator

      // const sendValue='1000000000000000000' // 1 ETH
      const sendValue = ethers.utils.parseEther('1') // 1 ETH

      beforeEach(async () => {
        deployer = (await getNamedAccounts()).deployer
        // const accounts = await ethers.getSigners() // Get accounts listed in network settings

        await deployments.fixture(['all'])

        fundMe = await ethers.getContract(
          'FundMe',
          deployer /* or use account[n] */
        )

        mockV3Aggregator = await ethers.getContract(
          'MockV3Aggregator',
          deployer
        )
      })

      describe('constructor', () => {
        it('sets the aggregator addresses correctly', async () => {
          const response = await fundMe.getPriceFeed()

          assert.equal(response, mockV3Aggregator.address)
        })
      })

      describe('fund', () => {
        it("fails if you don't send enough ETH", async () => {
          await expect(fundMe.fund()).to.be.revertedWith(
            'FundMe__Insufficient_ETH'
          )
        })

        it('keep track of the funder and amount', async () => {
          await fundMe.fund({ value: sendValue })

          const response = await fundMe.getAddressToAmountFunded(deployer)

          assert.equal(response.toString(), sendValue.toString())
        })

        it('add funder to funders list', async () => {
          await fundMe.fund({ value: sendValue })

          const funder = await fundMe.getFunder(0)

          assert.equal(funder, deployer)
        })
      })

      describe('withdraw', () => {
        beforeEach(async () => {
          await fundMe.fund({ value: sendValue })
        })

        it('can withdraw ETH from a single funder', async () => {
          // Arrange
          const startingFundMeBalance = await fundMe.provider.getBalance(
              fundMe.address
            ),
            startingDeployerBalance = await fundMe.provider.getBalance(deployer)

          // Act
          const transactionResponse = await fundMe.withdraw(),
            transactionReceipt = await transactionResponse.wait(1)

          // Gas Cost
          const { gasUsed, effectiveGasPrice } = transactionReceipt,
            gasCost = gasUsed.mul(effectiveGasPrice)

          // Assert
          const endingFundMeBalance = await fundMe.provider.getBalance(
              fundMe.address
            ),
            endingDeployerBalance = await fundMe.provider.getBalance(deployer)

          assert.equal(endingFundMeBalance, 0)
          assert.equal(
            startingFundMeBalance.add(startingDeployerBalance).toString(),
            endingDeployerBalance.add(gasCost).toString()
          )
        })

        it('can withdraw ETH from multiple funders', async () => {
          // Arrange
          const accounts = await ethers.getSigners()

          for (let i = 1; i < 6; i++) {
            const fundMeConnectedContract = await fundMe.connect(accounts[i])

            await fundMeConnectedContract.fund({ value: sendValue })
          }

          const startingFundMeBalance = await fundMe.provider.getBalance(
              fundMe.address
            ),
            startingDeployerBalance = await fundMe.provider.getBalance(deployer)

          // Act
          const transactionResponse = await fundMe.withdraw(),
            transactionReceipt = await transactionResponse.wait(1)

          // Gas Cost
          const { gasUsed, effectiveGasPrice } = transactionReceipt,
            gasCost = gasUsed.mul(effectiveGasPrice)

          // Assert
          const endingFundMeBalance = await fundMe.provider.getBalance(
              fundMe.address
            ),
            endingDeployerBalance = await fundMe.provider.getBalance(deployer)

          assert.equal(endingFundMeBalance, 0)
          assert.equal(
            startingFundMeBalance.add(startingDeployerBalance).toString(),
            endingDeployerBalance.add(gasCost).toString()
          )

          // Make sure funders information is reset properly
          await expect(fundMe.getFunder(0)).to.be.reverted

          for (let i = 1; i < 6; i++) {
            assert.equal(
              await fundMe.getAddressToAmountFunded(accounts[i].address),
              0
            )
          }
        })

        it('only allow the owner to withdraw', async () => {
          const accounts = await ethers.getSigners(),
            attacker = accounts[1],
            attackerConnectedContract = await fundMe.connect(attacker)

          await expect(attackerConnectedContract.withdraw()).to.be.revertedWith(
            'FundMe__NotOwner'
          )
        })
      })

      describe('cheaperWithdraw', () => {
        beforeEach(async () => {
          await fundMe.fund({ value: sendValue })
        })

        it('can withdraw ETH from a single funder', async () => {
          // Arrange
          const startingFundMeBalance = await fundMe.provider.getBalance(
              fundMe.address
            ),
            startingDeployerBalance = await fundMe.provider.getBalance(deployer)

          // Act
          const transactionResponse = await fundMe.cheaperWithdraw(),
            transactionReceipt = await transactionResponse.wait(1)

          // Gas Cost
          const { gasUsed, effectiveGasPrice } = transactionReceipt,
            gasCost = gasUsed.mul(effectiveGasPrice)

          // Assert
          const endingFundMeBalance = await fundMe.provider.getBalance(
              fundMe.address
            ),
            endingDeployerBalance = await fundMe.provider.getBalance(deployer)

          assert.equal(endingFundMeBalance, 0)
          assert.equal(
            startingFundMeBalance.add(startingDeployerBalance).toString(),
            endingDeployerBalance.add(gasCost).toString()
          )
        })

        it('can withdraw ETH from multiple funders', async () => {
          // Arrange
          const accounts = await ethers.getSigners()

          for (let i = 1; i < 6; i++) {
            const fundMeConnectedContract = await fundMe.connect(accounts[i])

            await fundMeConnectedContract.fund({ value: sendValue })
          }

          const startingFundMeBalance = await fundMe.provider.getBalance(
              fundMe.address
            ),
            startingDeployerBalance = await fundMe.provider.getBalance(deployer)

          // Act
          const transactionResponse = await fundMe.cheaperWithdraw(),
            transactionReceipt = await transactionResponse.wait(1)

          // Gas Cost
          const { gasUsed, effectiveGasPrice } = transactionReceipt,
            gasCost = gasUsed.mul(effectiveGasPrice)

          // Assert
          const endingFundMeBalance = await fundMe.provider.getBalance(
              fundMe.address
            ),
            endingDeployerBalance = await fundMe.provider.getBalance(deployer)

          assert.equal(endingFundMeBalance, 0)
          assert.equal(
            startingFundMeBalance.add(startingDeployerBalance).toString(),
            endingDeployerBalance.add(gasCost).toString()
          )

          // Make sure funders information is reset properly
          await expect(fundMe.getFunder(0)).to.be.reverted

          for (let i = 1; i < 6; i++) {
            assert.equal(
              await fundMe.getAddressToAmountFunded(accounts[i].address),
              0
            )
          }
        })

        it('only allow the owner to withdraw', async () => {
          const accounts = await ethers.getSigners(),
            attacker = accounts[1],
            attackerConnectedContract = await fundMe.connect(attacker)

          await expect(
            attackerConnectedContract.cheaperWithdraw()
          ).to.be.revertedWith('FundMe__NotOwner')
        })
      })
    })
