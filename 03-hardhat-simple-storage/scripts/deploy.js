// imports
const { ethers, run, network } = require('hardhat')

// async main
async function main() {
  const SimpleStorageFactory = await ethers.getContractFactory('SimpleStorage')

  console.log('Depolying contract...')

  const simpleStorage = await SimpleStorageFactory.deploy()
  await simpleStorage.deployed()

  console.log(`Deployed contract to: ${simpleStorage.address}`)

  if (4 === network.config.chainId && process.env.ETHERSCAN_API_KEY) {
    console.log('Waiting for block transactions...')
    await simpleStorage.deployTransaction.wait(6)

    await verify(simpleStorage.address, [])
  }

  const currentValue = await simpleStorage.retrieve()
  console.log(`Current Value is: ${currentValue}`)

  // Update the current value
  const transactionResponse = await simpleStorage.store(42)
  await transactionResponse.wait(1)

  const updatedValue = await simpleStorage.retrieve()
  console.log(`Updated Value is: ${updatedValue}`)
}

async function verify(address, constructorArguments) {
  console.log('Verifying contract...')

  try {
    await run('verify:verify', {
      address,
      constructorArguments,
    })
  } catch (ex) {
    if (ex.message.toLowerCase().includes('already verified')) {
      console.log('Already Verified!')
    } else {
      console.log(ex)
    }
  }
}

// main
main()
  .then(() => process.exit(0))
  .catch((err) => {
    console.error(err)

    process.exit(1)
  })
