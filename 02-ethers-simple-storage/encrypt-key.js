const ethers = require('ethers')
    , fs = require('fs-extra')

require('dotenv').config()

async function main() {
  const wallet = new ethers.Wallet(process.env.PRIVATE_KEY)
      , encryptedJsonKey = await wallet.encrypt
        ( process.env.PRIVATE_KEY_SECRET
        , process.env.PRIVATE_KEY
        )

  fs.writeFileSync('encrypted-key.json', encryptedJsonKey)

  console.log('Encrypted JSON key created.')
}

main()
  .then(() => process.exit(0))
  .catch(err => {
    console.error(err)

    process.exit(1)
  })
