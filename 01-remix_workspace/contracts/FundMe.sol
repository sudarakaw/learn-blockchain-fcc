// Get funds from users
// Withdraw funds
// Set a minumum funding value in USD

// SPDX-License-Identifier: BSD2
pragma solidity ^0.8.0;

import "./PriceConverter.sol";

error NotOwner();

contract FundMe {
    using PriceConverter for uint256;

    uint256 public constant MINIMUM_USD = 50 * 1e18;

    address[] public funders;
    mapping(address => uint256) public addressToAmountFunded;
    address public immutable i_owner;

    constructor() {
        i_owner = msg.sender;
    }

    function fund() public payable {
        require(msg.value.getConversionRate() >= MINIMUM_USD, "Didn't send enough"); // 10^18 
        // NOTE: failing above requirement revert the functions performed so far. Gas is still spent for them.

        funders.push(msg.sender);
        addressToAmountFunded[msg.sender] = msg.value;
    }

    function withdraw() public onlyOwner {
        //require(msg.sender == i_owner, "Sender is not i_owner");

        for(uint256 funderIndex = 0; funderIndex < funders.length; funderIndex = funderIndex + 1) {
            address funder = funders[funderIndex];

            addressToAmountFunded[funder] = 0;
        }

        // reset the funders array
        funders = new address[](0);

        // actually withdraw the funds

        /*
        // Method 1: transfer
        //
        // NOTE:
        // `msg.sender` has type of `address`
        // `payable(msg.sender)` has type of `payable address`
        // Only payable addresses can transfer blockchain token
        payable(msg.sender).transfer(address(this).balance);
        // Tranfer will automatically revert from here on failure

        // Method 2: send
        bool sendSuccess = payable(msg.sender).send(address(this).balance);
        // check status and manually revert.
        require(sendSuccess, "Send failed");
        */

        // Method 2: call
        (bool callSuccess, /*bytes memory  dataReturned*/) = payable(msg.sender).call{ value: address(this).balance }("");
        // check status and manually revert.
        require(callSuccess, "Call failed");
    }

    modifier onlyOwner {
        //require(msg.sender == i_owner, "Sender is not owner");

        if(msg.sender == i_owner) {
            revert NotOwner();
        }
        _; // run rest of the modified function
    }

    receive() external payable {
        fund();
    }

    fallback() external payable {
        fund();
    }
}