const { ethers, getNamedAccounts, network } = require('hardhat'),
  { developmentChains } = require('../../hardhat.config.helper'),
  { assert, expect } = require('chai')

developmentChains.includes(network.name)
  ? describe.skip
  : describe('FundMe', () => {
      let fundMe, deployer

      // const sendValue='1000000000000000000' // 1 ETH
      const sendValue = ethers.utils.parseEther('1') // 1 ETH

      beforeEach(async () => {
        deployer = (await getNamedAccounts()).deployer
        // const accounts = await ethers.getSigners() // Get accounts listed in network settings

        fundMe = await ethers.getContract(
          'FundMe',
          deployer /* or use account[n] */
        )
      })

      it('allow people to fund and withdraw', async () => {
        await fundMe.fund({ value: sendValue })
        await fundMe.withdraw()

        const endingBalance = await fundMe.provider.getBalance(fundMe.address)

        assert.equal(endingBalance.toString(), '0')
      })
    })
