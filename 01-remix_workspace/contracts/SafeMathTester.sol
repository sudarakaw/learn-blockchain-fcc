// SPDX-License-Identifier: BSD2
//pragma solidity ^0.6.0;
pragma solidity ^0.8.0;

contract SafeMathTester {
    uint8 public bigNumber = 255; // unchecked by default before 0.8.0

    function add() public {
        //bigNumber = bigNumber + 1;

        unchecked { bigNumber = bigNumber + 1; } // same as above line compiled before 0.8.0
    }
}