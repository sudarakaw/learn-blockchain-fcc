const ethers = require('ethers')
    , fs = require('fs-extra')

require('dotenv').config()

async function main() {
  const encryptedJson = fs.readFileSync('encrypted-key.json', 'utf8')
      , provider = new ethers.providers.JsonRpcProvider(process.env.RPC_URL || '')
      , abi = fs.readFileSync('./SimpleStorage.abi', 'utf8')
      , binary = fs.readFileSync('./SimpleStorage.bin', 'utf8')

  let wallet = null

  if(0 < encryptedJson.length && 0 < process.env.PRIVATE_KEY_SECRET?.length) {
    console.info('Using encrypted key...')

    wallet = new ethers.Wallet.fromEncryptedJsonSync
      ( encryptedJson
        , process.env.PRIVATE_KEY_SECRET || ''
      )

    wallet = await wallet.connect(provider)
  }
  else {
    console.info('Using plain-text key...')

    wallet = new ethers.Wallet(process.env.PRIVATE_KEY, provider)
  }


  const contractFactory = new ethers.ContractFactory(abi, binary, wallet)

  console.log('Deploying, please wait...')

  const contract = await contractFactory.deploy()
      // , transactionReceipt = await contract.deployTransaction.wait(1)

  await contract.deployTransaction.wait(1)
  console.log(`Contract Address: ${contract.address}`)

  // console.log('DEPLOYMENT TRANSACTION:')
  // console.log(contract.deployTransaction)
  // console.log('TRANSACTION RECEIPT:')
  // console.log(transactionReceipt)
  // console.log(contract)

  // console.log('LET\'S DEPLOY WITH ONLY TRANSACTION DATA')
  //
  // const nonce = await wallet.getTransactionCount()
  //     , tx =
  //       { nonce
  //       , 'gasPrice': 20000000000
  //       , 'gasLimit': 1000000
  //       , 'to': null
  //       , 'value': 0
  //       , 'data': `0x${binary}`
  //       , 'chainId': 1337
  //       }
  //     , sentTxResponse = await wallet.sendTransaction(tx)
  //
  // await sentTxResponse.wait(1)
  // console.log(sentTxResponse)

  const currentFavoriteNumber = await contract.retrieve()

  console.log(`Current Favorite Number: ${currentFavoriteNumber.toString()}`)

  const transactionResponse = await contract.store('7')
      , transactionReceipt = await transactionResponse.wait(1)
      , updatedFavoriteNumber = await contract.retrieve()

  console.log(`Updated Favorite Number: ${updatedFavoriteNumber.toString()}`)
}

main()
  .then(() => process.exit(0))
  .catch(err => {
    console.error(err)

    process.exit(1)
  })
