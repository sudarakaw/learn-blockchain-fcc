require('@nomiclabs/hardhat-waffle')
require('dotenv').config()
require('@nomiclabs/hardhat-etherscan')
require('hardhat-gas-reporter')
require('solidity-coverage')

require('./tasks/accounts')
require('./tasks/block-number')

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

/**
 * @type import('hardhat/config').HardhatUserConfig
 */

const RINKEBY_RPC_URL = process.env.RINKEBY_RPC_URL || '',
  PRIVATE_KEY = process.env.PRIVATE_KEY || '',
  ETHERSCAN_API_KEY = process.env.ETHERSCAN_API_KEY || '',
  COINMARKETCAP_API_KEY = process.env.COINMARKETCAP_API_KEY || ''

module.exports = {
  'defaultNetwork': 'hardhat',
  'networks': {
    'rinkeby': {
      'url': RINKEBY_RPC_URL,
      'accounts': [PRIVATE_KEY],
      'chainId': 4,
    },
    'localhost': {
      'url': 'http://127.0.0.1:8545',
      // 'accounts': [PRIVATE_KEY],
      'chainId': 31337,
    },
  },
  'etherscan': {
    'apiKey': ETHERSCAN_API_KEY,
  },
  'gasReporter': {
    'enabled': true,
    'outputFile': 'gas-report.txt',
    'noColors': true,
    'currency': 'USD',
    'coinmarketcap': COINMARKETCAP_API_KEY,
    'token': 'MATIC',
  },
  solidity: '0.8.8',
}
