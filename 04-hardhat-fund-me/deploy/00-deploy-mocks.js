// imports
const {
    developmentChains,
    DECIMALS,
    INITIAL_ANSWER,
  } = require('../hardhat.config.helper'),
  { network } = require('hardhat')

module.exports = async (/* hre */ { getNamedAccounts, deployments }) => {
  const { deploy, log } = deployments,
    { deployer } = await getNamedAccounts()

  console.log(`NAME: ${network.name}`)

  if (developmentChains.includes(network.name)) {
    log('Local network detected! Depolying mocks...')

    await deploy('MockV3Aggregator', {
      contract: 'MockV3Aggregator',
      from: deployer,
      args: [DECIMALS, INITIAL_ANSWER],
      log: true,
    })

    log('Mocks deployed!')
    log('----------------------------------------')
  }
}

module.exports.tags = ['all', 'mocks']
